**VegBat - a simulation tool for green façades and roofs**

This is a Trnsys TYPE that can be coupled with a multizone model and assess the effect of green envelopes on buildings.  
![experiment photo](TTV.jpg "experiment photo")

**Installation**

Copy the content of the bin folder within the TRNSYS folder.  
Note: the type is compiled for 64b installation of TRNSYS only. It might not work with 32b version.

**History**

2022-07-08 first release  
ADDED Dlls and proforma for greenWall type  
TODO add a project example coupled with Type56

**License**

Licensed by La Rochelle University/LaSIE under a BSD 3 license (https://opensource.org/licenses/BSD-3-Clause).


**References > to cite when using this component**

Djedjig, Rabah, Emmanuel Bozonnet, et Rafik Belarbi. « Analysis of thermal effects of vegetated envelopes: Integration of a validated model in a building energy simulation program ». Energy and Buildings 86 (janvier 2015): 93‑103. https://doi.org/10.1016/J.ENBUILD.2014.09.057.

Djedjig, Rabah, Emmanuel Bozonnet, et Rafik Belarbi. « Modeling green wall interactions with street canyons for building energy simulation in urban context ». Urban Climate 16 (juin 2016): 75‑85. https://doi.org/10.1016/J.UCLIM.2015.12.003.
